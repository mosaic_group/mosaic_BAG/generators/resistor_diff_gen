"""
Resistor layout generator
"""

from typing import *
from abs_templates_ec.resistor.core import ResArrayBase, TerminationCore
from abs_templates_ec.analog_core.substrate import SubstrateContact, DeepNWellRing
from bag.layout.util import BBox
from bag.layout.routing import TrackID, WireArray
from bag.layout.template import TemplateBase, TemplateDB

from sal.row import *
from sal.transistor import *

from .params import resistor_diff_layout_params


class resistor_cell(ResArrayBase):
    """A template for creating differential resistors.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
            the template database.
    lib_name : str
        the layout library name.
    params : dict[str, any]
        the parameter values.
    used_names : set[str]
        a set of already used cell names.
    **kwargs :
        dictionary of optional parameters.  See documentation of
        :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        # type: (TemplateDB, str, Dict[str, Any], Set[str], **Any) -> None
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary containing parameter descriptions.
        Override this method to return a dictionary from parameter names to descriptions.
        Returns
        -------
        param_info : Dict[str, str]
            dictionary from parameter name to description.
        """
        return dict(
            params='resistor_diff_layout_params object',
        )

    def draw_layout(self):
        params: resistor_diff_layout_params = self.params['params'].copy()

        # draw array
        if params.nx <= 0:
            raise ValueError('number of resistors in a row must be positive.')
        if params.ny % 2 != 0:
            raise ValueError('number of resistors in a column must be even')

        params.nx = params.nx + 2 * params.ndum
        params.ny = params.ny + 2 * params.ndum

        for key in ('idc', 'iac_rms', 'iac_peak'):
            if key in params.em_specs:
                params.em_specs[key] = params.em_specs[key] / params.ny

        draw_params = dict(
            l=params.l,
            w=params.w,
            sub_type=params.sub_type,
            threshold=params.threshold,
            nx=params.nx,
            ny=params.ny,
            em_specs=params.em_specs,
            top_layer=params.top_layer,
            res_type=params.res_type
        )

        self.draw_array(**draw_params)

        # connect row resistors
        vm_layer = self.bot_layer_id   # bottom metal resistor layer ID
        xm_layer = vm_layer + 1
        ym_layer = xm_layer + 1

        hl_warr_list = []
        hr_warr_list = []
        hl_snake_warr_list = []
        hr_snake_warr_list = []
        left_row = 0
        right_row = 1

        grid = self.grid
        res = grid.resolution
        pitch_vm = grid.get_track_pitch(vm_layer, unit_mode=True)
        res_layername = grid.tech_info.get_layer_name(vm_layer)

        vm_width = self.w_tracks[1] * 3
        for row_idx in range(params.ny):
            for col_idx in range(params.nx):
                ports_l = self.get_res_ports(row_idx, col_idx)
                if col_idx == params.nx - 1:
                    ports_r = self.get_res_ports(row_idx, col_idx)
                else:
                    ports_r = self.get_res_ports(row_idx, col_idx + 1)
                con_par = (col_idx + row_idx) % 2
                if col_idx != 0:
                    # Connect dummy resistors using M1
                    if row_idx == 0 or row_idx == params.ny - 1:
                        box_arr = port_prev[0].get_bbox_array(grid)
                        box_arr1 = ports_l[0].get_bbox_array(grid)
                        box = BBox(box_arr.left_unit - 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                   box_arr1.right_unit + 1 * pitch_vm, box_arr.top_unit + pitch_vm // 2, res,
                                   unit_mode=True)
                        self.add_rect(res_layername, box)
                        box_arr = self.get_res_ports(0, 0)[0].get_bbox_array(grid)
                        box_arr1 = self.get_res_ports(params.ny - 1, 0)[1].get_bbox_array(grid)
                        box = BBox(box_arr.left_unit - 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                   box_arr.left_unit + pitch_vm, box_arr1.top_unit + pitch_vm // 2, res,
                                   unit_mode=True)
                        self.add_rect(res_layername, box)
                        box_arr = port_prev[1].get_bbox_array(grid)
                        box_arr1 = ports_l[1].get_bbox_array(grid)
                        box = BBox(box_arr.left_unit - 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                   box_arr1.right_unit + 1 * pitch_vm, box_arr.top_unit + pitch_vm // 2, res,
                                   unit_mode=True)
                        self.add_rect(res_layername, box)
                        box_arr = self.get_res_ports(0, params.nx - 1)[0].get_bbox_array(grid)
                        box_arr1 = self.get_res_ports(params.ny - 1, params.nx - 1)[1].get_bbox_array(grid)
                        box = BBox(box_arr.right_unit - pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                   box_arr.right_unit + 1 * pitch_vm, box_arr1.top_unit + pitch_vm // 2, res,
                                   unit_mode=True)
                        self.add_rect(res_layername, box)
                    elif 1 < col_idx < params.nx - 1:
                        # Connect main resistors using horizontal M1 lines
                        if params.res_conn == 'p':
                            box_arr = port_prev[0].get_bbox_array(grid)
                            box_arr1 = ports_l[0].get_bbox_array(grid)
                            box = BBox(box_arr.left_unit - 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr1.right_unit + 0 * pitch_vm, box_arr.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(res_layername, box)
                            box_arr = self.get_res_ports(1, 1)[0].get_bbox_array(grid)
                            box_arr1 = self.get_res_ports(params.ny / 2 - 1, 1)[0].get_bbox_array(grid)
                            box = BBox(box_arr.left_unit - 2 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr.left_unit - 1 * pitch_vm, box_arr1.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(res_layername, box)
                            box_arr = self.get_res_ports(params.ny / 2, 1)[0].get_bbox_array(grid)
                            box_arr1 = self.get_res_ports(params.ny - 2, 1)[0].get_bbox_array(grid)
                            box = BBox(box_arr.left_unit - 2 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr.left_unit - 1 * pitch_vm, box_arr1.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(res_layername, box)
                            box_arr = port_prev[1].get_bbox_array(grid)
                            box_arr1 = ports_l[1].get_bbox_array(grid)
                            box = BBox(box_arr.left_unit - 0 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr1.right_unit + 1 * pitch_vm, box_arr.top_unit + pitch_vm // 2,
                                       res,
                                       unit_mode=True)
                            # Vertical lines
                            self.add_rect(res_layername, box)
                            box_arr = self.get_res_ports(1, params.nx - 2)[1].get_bbox_array(grid)
                            box_arr1 = self.get_res_ports(params.ny / 2 - 1, params.nx - 2)[1].get_bbox_array(grid)
                            box = BBox(box_arr.right_unit + 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr.right_unit + 2 * pitch_vm, box_arr1.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(res_layername, box)
                            box_arr = self.get_res_ports(params.ny / 2, params.nx - 2)[1].get_bbox_array(grid)
                            box_arr1 = self.get_res_ports(params.ny - 2, params.nx - 2)[1].get_bbox_array(grid)
                            box = BBox(box_arr.right_unit + 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr.right_unit + 2 * pitch_vm, box_arr1.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(res_layername, box)
                        else:
                            self.connect_wires([ports_l[con_par], port_prev[con_par]])
                port_prev = ports_l
                if params.res_conn == 'p':
                    if col_idx == 1:  # was 0, modified due to adding dummy resistors on the sides and
                                      # changing bot layer to M1
                        # column 0, left side
                        hl_warr_list.append(ports_l[1 - con_par])
                        if row_idx > 1:
                            # not in the first row
                            ports_l_below = self.get_res_ports(row_idx - 1, col_idx)
                            if row_idx != params.ny / 2 - 1:
                                vl_id = self.grid.coord_to_nearest_track(vm_layer, ports_l[con_par].upper,
                                                                         mode=-1, half_track=True)
                                v_tid = TrackID(vm_layer, vl_id, width=vm_width)
                                cur_list_l = [ports_l[1 - con_par], ports_l_below[con_par]]
                    if col_idx == params.nx - 3:
                        hr_warr_list.append(ports_r[1 - con_par])
                        if row_idx > 1:
                            ports_r_below = self.get_res_ports(row_idx - 1, col_idx + 1)
                            if row_idx != params.ny / 2 - 1:
                                vr_id = self.grid.coord_to_nearest_track(vm_layer, ports_r[con_par].middle,
                                                                         mode=1, half_track=True)
                                v_tid = TrackID(vm_layer, vr_id, width=vm_width)
                                if params.nx % 2 == 1:
                                    con_par = 1 - con_par
                                cur_list_r = [ports_r[con_par], ports_r_below[1 - con_par]]

                if params.res_conn == 's':  # Series connection needs modification, here as a place holder
                    if col_idx == 0:
                        # column 0, left side
                        hl_warr_list.append(ports_l[1 - con_par])
                        if row_idx > 0:
                            # not in the first row
                            ports_l_below = self.get_res_ports(row_idx - 1, col_idx)
                            if left_row == 1 and row_idx != params.ny / 2:
                                vl_id = self.grid.coord_to_nearest_track(vm_layer, ports_l[con_par].middle, mode=-1,
                                                                         half_track=True)
                                v_tid = TrackID(vm_layer, vl_id, width=vm_width)
                                cur_list_l = [ports_l[con_par], ports_l_below[1 - con_par]]
                                vl_warr = self.connect_to_tracks(cur_list_l, v_tid)
                    if col_idx == params.nx - 2:
                        hr_warr_list.append(ports_r[1 - con_par])
                        if row_idx > 0:
                            ports_r_below = self.get_res_ports(row_idx - 1, col_idx + 1)
                            if right_row == 1 and row_idx != params.ny / 2:
                                vr_id = self.grid.coord_to_nearest_track(vm_layer, ports_r[con_par].middle, mode=1,
                                                                         half_track=True)
                                v_tid = TrackID(vm_layer, vr_id, width=vm_width)
                                cur_list_r = [ports_r[con_par], ports_r_below[1 - con_par]]
                                vr_warr = self.connect_to_tracks(cur_list_r, v_tid)
            if row_idx > 0:
                left_row = 1 - left_row
                right_row = 1 - right_row

        # Add Pins
        ports_bottom_left = self.get_res_ports(1, 1)  # (0,0) changed due to adding a row of dummy resistors around
        con_par = (0 + 0) % 2
        vl_id = self.grid.coord_to_nearest_track(ym_layer, ports_bottom_left[con_par].middle, mode=-1, half_track=True)
        self.add_pin("in1", ports_bottom_left[con_par], show=True)

        ports_bottom_right = self.get_res_ports(params.ny / 2 - 1, ((params.ny / 2) % 2) * (params.nx - 3) + 1)
        con_par = int((params.nx - 1 + ((params.ny - 2) / 2) - 1)) % 2
        if (params.nx % 2) == 1 and ((params.ny - 2) / 2) % 2 == 0:
            con_par = 1 - con_par
        if (params.nx % 2) == 0 and params.res_conn == 'p':
            con_par = 1 - con_par

        con_par = 1 - con_par
        if params.res_conn == 'p':
            ports_bottom_right = self.get_res_ports(params.ny / 2 - 1, 1)
            if ((params.ny - 2) / 2) % 2 == 0 and params.nx % 2 == 1:
                con_par = 1 - con_par
        self.add_pin("in2", ports_bottom_right[con_par], show=True)

        ports_top_right = self.get_res_ports(params.ny / 2, ((params.ny / 2) % 2) * (params.nx - 3) + 1)
        if params.res_conn == 'p':
            ports_top_right = self.get_res_ports(params.ny / 2, 1)

        con_par = 1 - con_par
        self.add_pin("in4", ports_top_right[con_par], show=True)

        ports_top_left = self.get_res_ports(params.ny - 2, 1)  # (params.ny-1, 0) due to dummies
        if params.res_conn == 'p':
            ports_top_left = self.get_res_ports(params.ny - 2, 1)
        con_par = ((params.ny - 2) - 1) % 2
        self.add_pin("in3", ports_top_left[con_par], show=True)
        self.add_pin('VDD', self.get_res_ports(0, 0)[0], label='VDD', show=True)

        # Set the size
        self.size = self.grid.get_size_tuple(layer_id=5, width=self.array_box.width, height=self.array_box.height,
                                             round_up=True, unit_mode=False)

        # create schematic parameters
        self._sch_params = dict(
            params=self.params['params'],
        )


class layout(TemplateBase):
    """
    This class corrects the grid information so that wire arrays can be used directly from top cell to connect this resistor_diff cell.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='resistor_array_layout_params parameter object',
        )

    def draw_layout(self):
        params: resistor_diff_layout_params = self.params['params']

        res_cell_master = self.new_template(temp_cls=resistor_cell, params=dict(params=params))

        res_inst = self.add_instance(res_cell_master, inst_name='XRES', loc=(0, 0), unit_mode=True)

        for name in ['in1', 'in2', 'in3', 'in4','VDD']:
            port = res_inst.get_port(name)
            pin = port.get_pins(res_cell_master.bot_layer_id)[0]
            bbox = pin.get_bbox_array(res_cell_master.grid)
            xm_layer = res_cell_master.bot_layer_id + 1
            xm_layername = self.grid.tech_info.get_layer_name(xm_layer)
            xm_layer_pitch = self.grid.get_track_pitch(layer_id=xm_layer, unit_mode=True)
            # Since there is a difference in routing grids of 'self' and 'res_master' object
            # we create a wire array in 'self' object's grid, that is close to the actual pin drawn in 'res_master' object's grid
            # and connect them. We do this by considering the ports of res_core, so as to avoid unneccesary shorts between same metal layers
            if self.grid.get_direction(xm_layer) == 'x':
                tr_num = self.grid.coord_to_nearest_track(layer_id= xm_layer, coord= bbox.top_unit, mode= 2, unit_mode= True)
                tr_num += 2  # To make sure we don't violate xm_layer-to-xm_layer spacing DRC rules
                warr_pin = WireArray(TrackID(xm_layer, tr_num),lower= bbox.left_unit, upper= bbox.right_unit, res= self.grid.resolution, unit_mode= True)
                vertical_conn_wire = BBox(left= warr_pin.upper_unit - xm_layer_pitch, right= warr_pin.upper_unit,
                                            bottom= bbox.bottom_unit,top= self.grid.track_to_coord(warr_pin.layer_id, tr_num, unit_mode=True), 
                                            resolution= self.grid.resolution, unit_mode= True )
            else:
                raise Exception("Currently only vertically drawn resistor arrays are supported. Hence metal layer 2 must be oriented in X direction to make connections")     
            self.add_pin(name, warr_pin, label=name, show= True)
            self.add_rect(xm_layername, bbox=warr_pin.get_bbox_array(self.grid))
            self.add_rect(xm_layername, bbox=vertical_conn_wire)

        self.size = res_cell_master.size
        # create schematic parameters
        self._sch_params = dict(
            params=self.params['params'],
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params

class resistor_diff(layout):
    """
    Class to be used as template in higher level layouts
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
