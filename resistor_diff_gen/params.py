#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.testbench_base import TestbenchBase
from sal.testbench_params import *


@dataclass
class resistor_diff_layout_params(LayoutParamsBase):
    """
    Parameter class for resistor_diff_gen

    Args:
    ----

    l : float
        unit resistor length, in meters.

    w: float
        unit resistor width, in meters.

    io_width_ntr: int
        I/O width, in number of tracks.

    sub_type: str
        the substrate type.

    threshold: str
        the substrate threshold flavor.

    nx: int
        number of resistors in a row.  Must be even.

    ny: int
        number of resistors in a column.

    res_type: str
        the resistor type.

    em_specs: Dict
        EM specifications for the termination network.

    top_layer: int
        The top level metal layer.  None for primitive template.

    delete_vdd_pin: bool
        True to delete in schematic for passing LVS on a stand-alone cell

    res_conn: str
        Type of resistor connection

    ndum: int
        number of dummy resistors
    """

    l: float
    w: float
    io_width_ntr: int
    sub_type: str
    threshold: str
    nx: int
    ny: int
    res_type: str
    em_specs: Dict
    top_layer: int
    delete_vdd_pin: bool
    res_conn: str
    ndum: int

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> resistor_diff_layout_params:
        return resistor_diff_layout_params(
            l=50,
            w=25,
            io_width_ntr=1,
            sub_type='ntap',
            threshold='standard',
            nx=8,
            ny=2,
            res_type='standard',
            em_specs={},
            delete_vdd_pin=False,
            top_layer=5,
            res_conn='p',
            ndum=1,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> resistor_diff_layout_params:
        return resistor_diff_layout_params(
            l=50 * min_lch,
            w=25 * min_lch,
            io_width_ntr=1,
            sub_type='ntap',
            threshold='standard',
            nx=8,
            ny=2,
            res_type='standard',
            em_specs={},
            delete_vdd_pin=False,
            top_layer=5,
            res_conn='p',
            ndum=1,
        )


@dataclass
class resistor_diff_params(GeneratorParamsBase):
    layout_parameters: resistor_diff_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> resistor_diff_params:
        return resistor_diff_params(
            layout_parameters=resistor_diff_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
