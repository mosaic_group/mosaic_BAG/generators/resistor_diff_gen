v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 210 -410 230 -410 {
lab=in1}
N 290 -410 310 -410 {
lab=in2}
N 210 -260 230 -260 {
lab=in3}
N 290 -260 310 -260 {
lab=in4}
N 260 -240 260 -210 {
lab=VDD}
N 210 -110 230 -110 {
lab=VDD}
N 290 -110 310 -110 {
lab=VDD}
N 260 -390 260 -360 {}
N 260 -90 260 -60 {}
C {devices/iopin.sym} 110 -350 2 0 {name=p2 lab=in1}
C {devices/iopin.sym} 110 -410 2 0 {name=p3 lab=VDD}
C {devices/lab_pin.sym} 210 -410 0 0 {name=l4 sig_type=std_logic lab=in1}
C {BAG_prim/res_standard/res_standard.sym} 260 -410 1 0 {name=X_R_TOP
w=1u
l=2u
model=res_standard
spiceprefix=X
}
C {devices/iopin.sym} 110 -330 2 0 {name=p1 lab=in2}
C {devices/iopin.sym} 110 -280 2 0 {name=p4 lab=in3}
C {devices/iopin.sym} 110 -260 2 0 {name=p5 lab=in4}
C {devices/lab_pin.sym} 310 -410 2 0 {name=l1 sig_type=std_logic lab=in2}
C {devices/lab_pin.sym} 260 -360 3 0 {name=l2 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 210 -260 0 0 {name=l3 sig_type=std_logic lab=in3}
C {BAG_prim/res_standard/res_standard.sym} 260 -260 1 0 {name=X_R_BOT
w=1u
l=2u
model=res_standard
spiceprefix=X
}
C {devices/lab_pin.sym} 310 -260 2 0 {name=l5 sig_type=std_logic lab=in4}
C {devices/lab_pin.sym} 260 -210 3 0 {name=l6 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 210 -110 0 0 {name=l7 sig_type=std_logic lab=VDD}
C {BAG_prim/res_standard/res_standard.sym} 260 -110 1 0 {name=XDUM
w=1u
l=2u
model=res_standard
spiceprefix=X
}
C {devices/lab_pin.sym} 310 -110 2 0 {name=l8 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 260 -60 3 0 {name=l9 sig_type=std_logic lab=VDD}
